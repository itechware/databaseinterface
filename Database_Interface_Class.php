<?php
#Version 1.0
#Description: Automatically syncs database to matched class following naming conventions.  Class name should refer to Database_Table.
require_once('Database_Class.php');
require_once('Helper_Class.php');

class database_interface{
	
	public static $last_id_created;//last id inserted using this class is put here~
	
	public static $debug = false;
	
	private static function getTableNames(){
		//database :: connect();
		$sql = 'Show tables';
		database :: resetSelectArray();
		$tableData = database :: selectSql($sql);
		return $tableData;
	}
	
	public static function doesTableExists($class){
		$className = get_class($class);
		$tableData = self::getTableNames();
		$foundMatch=false;
		foreach($tableData as $key=>$value){
			foreach($value as $tableName){
				//echo '<br/>key='.$key.':'.$tableName;
				if($tableName==$className){
					$foundMatch=true;
					break 2; //break out of the two nested loops they are not needed anymore.
				}
			}
		}
		
		return $foundMatch; //returns if the table was in the list or not.
	}

	public static function createNew($class,$useTemplate=false){
		$className = get_class($class); //Ex.) PSUSA_client
		//$classVars = get_class_vars($className); //Ex.) firstname, lastname, etc
		$classVars = get_object_vars($class);//in case there are fields that need to be left NULL for lack of information.  Makes this more flexible.
		//$classParts = explode("_",$className);// Ex.) Separates PSUSA & client
		//$classParts = array_slice($classParts,1); 
		//$tableName = implode("_",$classParts);
		
		$tableName = $className; //tablename is always = to the classname
		
		foreach($classVars as $key => $value){
			//if($key!='id'){//ignore primary key references (we are going to allow primary key references if they exists)
				if($useTemplate==true){
					$tempvalue = $value['default'];
				}else{
					$tempvalue = $class->$key;
					if($tempvalue!='' && is_array($tempvalue)==false && is_object($tempvalue)==false){
						$tempvalue = mysqli_real_escape_string (database::$databaseConnection,$tempvalue);
						$tempvalue = htmlspecialchars($tempvalue);
					}
				}
				$data[$key]=$tempvalue;
			//}
		}
		
		$sql = database :: createSql($data,$tableName);
		//database :: connect();
		database :: begin();//start transactions
		$worked = database :: runSql($sql);
		self :: $last_id_created = database :: returnLastInsertedId();
		if($worked == true){
			database :: commit();//end transactions
			return true;
		}else{
			return false;
		}
	}
	
	//Deletes all rows by a factor.  Example -> DELETE FROM table WHERE factorName = factorValue;
	public static function deleteAllByFactor($class,$factorName,$factorValue){
		$className = get_class($class);
		$tableName = $className; //table name = class name always
		
		database :: begin();//start transactions
		$worked = database :: runSql("DELETE FROM ".$tableName." WHERE ".$factorName."='".$factorValue."';");
		
		if($worked==true){
			database :: commit();//commit changes.
			return true;
		}else{
			return false;
		}
	}
	
	public static function delete($class){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
		//$classParts = explode("_",$className);//separates the underscores
		//$classParts = array_slice($classParts,1);//takes everything from the right of the underscore.
		//$tableName = implode("_",$classParts);//put it back together in the order of the new sliced array.
		
		$tableName = $className; //table name = class name always
		
		if(isset($class->id)){
			$id = $class->id;//should be an id for it to delete
		}else{
			return false;
		}
		
		database :: begin();//start transactions
		$worked = database :: runSql("DELETE FROM ".$tableName." WHERE id='".$id."';");
		
		if($worked==true){
			database :: commit();//commit changes.
			return true;
		}else{
			return false;
		}
	}

	public static function update($class){
		$className = get_class($class);
		//$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
		$classVars = get_object_vars($class); //using object_vars here allows updating ONLY of what is neccesary.
		//$classParts = explode("_",$className);//separates the underscores
		//$classParts = array_slice($classParts,1);//takes everything from the right of the underscore.
		//$tableName = implode("_",$classParts);//put it back together in the order of the new sliced array.
		
		$tableName = $className;//table name = class name Always!
		
		if (self::$debug == true){
			echo '<br/>Table Updating='.$tableName.'<br/>';
		}
		
		$data['id']=$class->id;//an id should have been set, lets add it into the data so the update command will work.
		foreach($classVars as $key => $value){
				$tempvalue = $class->$key;
				$tempvalue = mysqli_real_escape_string (database::$databaseConnection,$tempvalue);
				$tempvalue = htmlspecialchars($tempvalue);
				$data[$key]=$tempvalue;
		}
		if (self::$debug == true){
			echo '<b>Attempting Update</b><br/>';
		}
		database :: begin();//start transactions
		if (self::$debug == true){
			database :: $debug=true;//turn on debug
		}
		database :: update($data, $tableName, '');
		database :: commit();//commit changes.
	}

	public static function getPrimaryKeyIdUsingUniqueColumnInTable($table,$column,$columnValue){
		$sql = "SELECT id FROM ".$table." WHERE ".$column."='".$columnValue."';";
		database :: resetSelectArray();
		$data = database :: selectSql($sql);
		if(isset($data[0]['id'])){
			return $data[0]['id'];//should only ever return one value.
		}else{
			return '-1';
		}
	}
	
	public static function instantiate($class){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
		
		//$classParts = explode("_",$className);//separates the underscores
		//$classParts = array_slice($classParts,1);//takes everything from the right of the underscore.
		//$tableName = implode("_",$classParts);//put it back together in the order of the new sliced array.
		$tableName = $className;
		
		//database :: connect();
		$sql = 'SELECT * FROM '.$tableName.' WHERE id='.$class->id.';';
		database :: resetSelectArray();
		$data = database :: selectSql($sql);//run the SQL statement against the correct table, only returns back 1 result for field name checks.
		
		//***Load all the single-point data***
		foreach($data[0] as $key => $value){
			foreach($classVars as $classKeys => $classValues){
				if($classKeys == $key){
					$value = mysqli_real_escape_string (database::$databaseConnection,$value);//replace any apostrophies in names they will conflict with eval~
					$class->$classKeys=$value;//set the class objects property accordingly
				}
			}
		}

		//***Load all the _Ids data***
		foreach($classVars as $key => $value){
			if(strpos($key,"_Ids")>0){
				//We found an _Ids attribute of the class
				$arrayPart = explode("_",$key);
				$subTableName = $arrayPart[0];
				$subTableName = $tableName.'_'.$subTableName;
				$sql = "Select id FROM ".$subTableName.' WHERE '.$tableName.'_id='.$class->id;
				database :: resetSelectArray();
				$data = database :: selectSql($sql);
				$class->$key = array();//make sure it's an empty array first in case there is values here.
				foreach($data as $array){
					foreach($array as $arrayKey => $arrayValue){
						//should only be id's here, so need to do if check on $arrayKey
						array_push($class->$key,$arrayValue);
					}
				}
			}
		}
		
		if (self::$debug == true){
			echo '<br/>Instantiate Test:<br/>';
			Helper :: pretty_var($class);
		}
		return $class;
	}
	
	public static function createTable($class){
		//*****NOTE: createTable functions NEED TO BE CREATED IN ORDER based on needs of Foreign_Keys or this won't work *****
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		$temp = explode("_",$tableName);
		$tableClassPrefix = $temp[0];//the left always contains the class prefix like PSUSA_ or MYPROGRAM_ etc...
		
		if (self::$debug == true){
			echo $tableName.'<br/><br/>';
		}
		
		$sql="CREATE TABLE IF NOT EXISTS `".$tableName."`(`id` int(11) NOT NULL AUTO_INCREMENT,"; //Starting SQL CreateTable Statement
		
		// CREATE TABLE IF NOT EXISTS `test` (
		//   `id` int(11) NOT NULL AUTO_INCREMENT,
		//   `address` varchar(255) NOT NULL,
		//   `firstname` varchar(255) NOT NULL,
		//   `lastname` varchar(255) NOT NULL,
		//   `coordinate_id` int(11) NOT NULL,
		//   PRIMARY KEY (`id`),
		//   KEY `coordinate_id` (`coordinate_id`)
		// ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		// 
		// -- Constraints for table `test`
		// --
		// ALTER TABLE `test`
		//   ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinatetest` (`id`) ON UPDATE CASCADE;
	
		$foreignKeys = array(); //starts out an empty array.
		
		foreach($classVars as $key => $value){
			$columnName = $key;
			$columnType = $value['type'];//take first item of array which stores the type.
			
			$isForeignKey = "";//by default it should have no value
			$foreignKeyName = "";//reset name
			
			$temp = explode("_",$columnName);
			$identifier = $temp[1];//right to underscore might be an _id, this signifies a foreign key.
			$identifierName = $temp[0];//just the name of the foreign key without _id
			
			if (self::$debug == true){
				echo $columnName.':'.$columnType.'<br/>';
			}
			
			$sql .= '`'.$columnName.'`'.' '.$columnType.',';
			
			if($identifier=="id"){ //this means it's a foreign key
				$foreignKeys[] = $identifierName;
			}
		}
		//Add Primary Key & Index References
		$sql .= "PRIMARY KEY (`id`),";
		foreach($foreignKeys as $key => $value){
			$sql .= "KEY `".$value."_id` (`".$value."_id`),";
		}
		$sql = rtrim($sql,',');//remove trailing comma
		$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;';//Close create table statement, make sure Inno_DB so Foreign Keys Work.
		
		if (self::$debug == true){
			echo '<br/><br/>'.$sql.'<br/><br/>';
		}
		$worked = Database::runSql($sql);//attempt to create table
		
		if($worked==false){
			return false;
		}
		
		$sql = '';//reset sql again.
		// Note: **Foreign Key Names must be UNIQUE since they are global to the database**
		// SAMPLE FOREIGN KEY ALTER STATEMENT
		// ALTER TABLE `test`
		// ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinatetest` (`id`) ON UPDATE CASCADE;
		foreach($foreignKeys as $key => $value){
			$sql = "ALTER TABLE `".$tableName."` ADD CONSTRAINT `".$tableName."_".$value."_ibfk` FOREIGN KEY (`".$value."_id`) REFERENCES `".$tableClassPrefix."_".$value."` (`id`) ON UPDATE CASCADE;";
			if (self::$debug == true){
				echo '<br/><br/>'.$sql;
			}
			$worked = Database::runSql($sql);//attempt to run alter command
			if($worked==false){
				return false;
			}
		}
		
		if (self::$debug == true){
			echo '<br/>createTable() -- Class Instantiated Test:<br/>';
			Helper :: pretty_var($class);
		}
		
		return true;//means everything went okay!
	}
	
	public static function dropTable($class){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		$sql = "DROP TABLE IF EXISTS ".$tableName.";";
		$worked = Database::runSql($sql);
		
		if (self::$debug == true){
			echo '<br/>'.$sql;
			echo '<br/>dropTable() -- Class Instantiated Test:<br/>';
			Helper :: pretty_var($class);
		}
		
		return $worked;
	}
	
	public static function numberOf($class){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		$sql = "SELECT count(id) AS `numberOf` FROM ".$tableName;//have the database do the count
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		$numberOf = $result[0]['numberOf'];//result 0 with column identifier numberOf
		return $numberOf;
	}
	
	public static function numberOfByFactor($class,$factorName,$factorValue){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		$sql = "SELECT count(id) AS `numberOf` FROM ".$tableName." WHERE ".$factorName."='".$factorValue."';";//have the database do the count
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		$numberOf = $result[0]['numberOf'];//result 0 with column identifier numberOf
		return $numberOf;
	}
	
	//This function handles more than 1 factor by passing an array with key=factorName value=factorValue
	public static function numberOfByFactors($class,$factors){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		$sql = "SELECT count(id) AS `numberOf` FROM ".$tableName.' WHERE ';
		foreach($factors as $key=>$value){
			$sql.=$key."='".$value."'"." AND ";//have the database do the count
		}
		$sql = substr($sql, 0, -5);//remove last 5 characters
		$sql .= ';';
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		$numberOf = $result[0]['numberOf'];//result 0 with column identifier numberOf
		return $numberOf;
	}
	
	public static function selectAll($class,$limit=0){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		if($limit==0){
			$sql = "SELECT * FROM ".$tableName.";";//have the database do the count
		}else{
			$sql = "SELECT * FROM ".$tableName." LIMIT ".$limit.";";
		}
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		return $result;
	}
	
	public static function selectAllByFactor($class,$factorName,$factorValue,$limit=0){
		$className = get_class($class);
		//$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		if($limit==0){
			$sql = "SELECT * FROM ".$tableName." WHERE ".$factorName."=".$factorValue.";";//have the database do the count
		}else{
			$sql = "SELECT * FROM ".$tableName." WHERE ".$factorName."=".$factorValue." LIMIT ".$limit.";";
		}
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		return $result;
	}
	
	public static function selectAllByFactors($class,$factors){
		$className = get_class($class);
		$classVars = get_class_vars($className);//gets all the member variables of the class, including _Ids types.
	
		$tableName = $className;//the name of the table will be the same as the class name.
		
		$sql = "SELECT * FROM ".$tableName.' WHERE ';
		foreach($factors as $key=>$value){
			$sql.=$key."='".$value."'"." AND ";//have the database do the count
		}
		$sql = substr($sql, 0, -5);//remove last 5 characters
		$sql .= ';';
		
		Database::resetSelectArray();
		$result = Database :: selectSql($sql);
		
		return $result;
	}
	
	//Version 1.0.0
	//Requires GET paramter of restore=true or restore=false to work.
	//restore=true will attempt to take previous data and put it into the new setup.
	//Warning: This may cause an infinite loop and is still in BETA.
	//Will only work if you added to the class not 'took away'.
	public static function install(){
		set_time_limit(30);//30 seconds is longest this script should ever run
	
		$memory_limit = ini_get('memory_limit');
		echo '<b>MemLimit:'.$memory_limit.'</b>';
	
		$start_memory = memory_get_usage(true);//for real reference
		echo '<br/>Memory Start='.$start_memory.'<br/>';
	
		//LOCAL FUNCTIONS ****************************************************************
		function createTableRecursive($classInstance){
			global $class_container;
			$className = get_class($classInstance);
			$classvars = get_class_vars($className);//get the list of variables for the class
			$classNameParts = explode("_",$className);
			$appName = $classNameParts[0];//PSUSA_
			$baseClassName = $classNameParts[1];//_someclass
		
			echo '<br/>Recursive Create For:'.$className;
			foreach($classvars as $key=>$value){
				$variableParts = explode("_",$key);
				if($variableParts[1]=="id"){//this is a foreign key, attempt to create table for this.
					$classNameForCreate = $appName."_".$variableParts[0];
					$tempClass = new $classNameForCreate;
					if(Database_Interface::doesTableExists($tempClass)){
						continue;//then don't worry about it
					}
					createTableRecursive($tempClass);//otherwise drop the class.
				}
			}
		
			//Database_Interface::$debug=true;
			//Database::$debug=true;
			$worked = Database_Interface::createTable($classInstance);//attempt to drop table of the class instance
			if($worked==true){
				echo '<br/><b>Created '.$className.'</b>';
			}else{
				die('<br/>Create Failed on:'.$className);
			}
		}
	
		// *******************************************************************************
	
		//SAFETY TRIGGER
		if(!isset($_GET['restore'])){
			die('Please use restore=true or restore=false parameter for your safety');
		}
	
		// CREATE CLASS DESCRIPTORS ******************************************************
		$classes = scandir('classes');
		foreach($classes as $file){
			if($file!='.' and $file!='..'){
				echo '<br/>Required:'.$file;
				require_once('classes/'.$file);
			}
		}
	
		Database::connect();
	
		//Create NEW instantiated class objects.
		$class_container = array();
		foreach($classes as $className){
			$theClass = basename($className);
			if($theClass!='.' && $theClass!='..'){
				$theClass= explode("_",$theClass);
				$theClass = $theClass[0].'_'.strtolower($theClass[1]);
				echo '<br/>Created Instance Of :'.$theClass;
				$class_container[$theClass] = new $theClass;
			}
		}
		//Helper::pretty_var($class_container);
		$classCount = count($class_container);//used for all looping from here out.
		echo '<br/>Classcount='.$classCount;
		// *******************************************************************************
	
		$new_memory = memory_get_usage(true) - $start_memory;
		echo '<br/><br/>Memory Used So Far For Classes:'.$new_memory;
		echo '<br/>Memory Used For Class_Container='.Helper::sizeof_var($class_container).'<br/>';
		
		//BACKUP DATA SECTION ************************************************************
		if($_GET['restore']=="true"){
			foreach($class_container as $className=>$classInstance){
				echo '<br/>Making Backup Of:'.$className.' ...';
				if(Database_Interface::doesTableExists($classInstance)){ //if the table does not exist there is nothing to backup.			
					$results = Database_Interface::selectAll($classInstance);//returns all known of the class type from database.
					foreach($results as $classDataIndex=>$classFieldsArray){
						foreach($classFieldsArray as $key=>$value){
							$class_backup[$className][$classDataIndex][$key] = $value;
							//TODO: Because of memory limitations, may need to write this to file.
						}
					} 
				}else{
					echo '<br/><b>Could not make backup of Table:'.$className.' , because table does not exists!</b>';
				}
			}	
		}
	
		$new_memory = memory_get_usage(true) - $start_memory;
		echo '<br/><br/>Memory Used So Far (Backup):'.$new_memory;
		echo '<br/>Memory Used For class_backup='.Helper::sizeof_var($class_backup).'<br/>';
		echo '<br/>Class BackUp Data:<br/>';
		Helper::pretty_var($class_backup);
		// *******************************************************************************
	
		//die('<br/>End Of Test');
	
		//DROP TABLE SECTION *************************************************************
		echo '<br/>Drop Tables';
		$failOccured=false;
		$skip=false;//by default
		for($i=0;$i<$classCount;$i++){
			$keysArray = array_keys($class_container);//returns an array same size but just the keys
			$className = $keysArray[$i];//set the key name
			$classInstance = $class_container[$className];//set the class instance by className
			//Check to see if the table even exists
			if(!Database_Interface::doesTableExists($classInstance)){ //If it does not exists then skip this class
				echo '<br/>'.$i.' - Table: '.$className.' already dropped';
				$skip=true;
			}
		
			if($skip==false){
				//Check the class that the foreign key belongs to and if it has NO foreign keys then drop, recursive out and drop until return.
				$worked = Database_Interface::dropTable($classInstance);//attempt to drop table of the class instance
				if($worked==true){
					echo '<br/>'.$i.'<b> - Dropped '.$className.'</b>';
				}else{
					echo '<br/>'.$i.' - Drop Failed on:'.$className;
					$failOccured=true;
				}
			}else{
				$skip=false;//since we don't know if we need to skip anymore.
			}
		
			if($failOccured==true && $i==$classCount-1){
				$i=-1;//reset the loop until no failures occur.
				$failOccured=false;
				echo '<br/>Loop and Drop Again...';
			} 
		}
		// *******************************************************************************
	
		//die('<br/>End Of Test');
	
		//CREATE TABLE SECTION************************************************************
		 echo '<br/>Create Tables';
		 for($i=0;$i<$classCount;$i++){
			$keysArray = array_keys($class_container);//returns an array same size but just the keys
			$className = $keysArray[$i];//set the key name
			$classInstance = $class_container[$className];//set the class instance by className
			//Check to see if the table even exists
			if(Database_Interface::doesTableExists($classInstance)){ //If it does not exists then skip this class
				echo '<br/>Table: '.$className.' already created';
				continue;//skip to next class since it's already been dropped either by recursion or manually.
			}
			//Check the class that the foreign key belongs to and if it has NO foreign keys then drop, recursive out and drop until return.
			createTableRecursive($classInstance);
		}
		//********************************************************************************
	
		//TODO: This function is very bad form since it allows for a possible infinite loop under poor conditions, make this better later.
		function restoreDataRecursive($data)
		{
			echo '<br/>Recursive Loop Start...';
			foreach($data as $classInstance){
				$className = get_class($classInstance);
				$worked = Database_Interface::createNew($classInstance);//put it back in the database the way it was, less possible missing values now.
				if($worked==false){
					echo '<br/><span style="color:red">Failed to Restore:'.$className.'</span>';
					$failedInstances[]=$classInstance;
					//If this is the case probably a foreign key error, say there was an error and force the loop but only for the error items.
				}else{
					echo '<br/><b>Restore Success:'.$className.'</b>';
				}
			}
			if(isset($failedInstances)){
				echo '<br/>Starting Recursive Loop Again...';
				restoreDataRecursive($failedInstances);//if there is indeed a variable set for $failedInstances then we need to keep going.  Potential for infinite loop.
			}
		}
	
		//RESTORE DATA SECTION ***********************************************************
		//TODO: Restore Order Needs To Be Done Correctly Based On Foreign Keys
		//TODO: Do to memory restrictions may need to restore data from backup_file.
		if($_GET['restore']=="true"){
			//Example: $class_backup[$className][$classDataIndex][$key] = $value;
			if(is_array($class_backup)){ //has to be items in the table or this variable has to even exists to do anything with it.
				foreach($class_backup as $className=>$classDataArray){
					echo '<br/>Attempting Restore:'.$className;
					foreach($classDataArray as $classDataIndex=>$classDataIndexArray){//go through all stored classes in the database
						$classInstance = new $className; //create a new empty temporary class instance.
						foreach($classDataIndexArray as $key=>$value){//go through the field names and values for them.
							echo '<br/>'.$key.'='.$value;
							$classInstance->$key = $value;//set the instance properties accordingly.
						}
						$worked = Database_Interface::createNew($classInstance);//put it back in the database the way it was, less possible missing values now.
						if($worked==false){
							echo '<br/><span style="color:red">Failed to Restore:'.$className.'</span>';
							$failedInstances[]=$classInstance;
							//If this is the case probably a foreign key error, say there was an error and force the loop but only for the error items.
						}else{
							echo '<br/><b>Restore Success:'.$className.'</b>';
						}
					}
				}
				if(isset($failedInstances)){//only attempt recursion on failures.
					restoreDataRecursive($failedInstances);//this will go through the elements that failed attempting to create them until no more errors occur.
				}
			}else{
				echo '<br/><b>Nothing to restore!</b>';
			}
		}
	
		// *******************************************************************************
	 
		 die('<BR/>End Of Script');
	}

}
?>