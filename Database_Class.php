<?php
require_once('Database_Config_Class.php');//require the config information.
class database{

	public static $databaseConnection;
	public static $selectResult;
	public static $debug=false;
	
	public static function runSql($sql){
		$success = self :: $databaseConnection -> query($sql);
		if($success === FALSE){
			if(self::$debug == true){
				die('The following SQL failed to work: '.$sql.'<br />'.self :: $databaseConnection -> error);
			}
			return false;
		}
		return true;
	}
	
	public static function runMultiSql($sql){
		$success = self :: $databaseConnection -> multi_query($sql);
		if($success === FALSE){
			die('The following SQL failed to work: '.$sql.'<br />'.self :: $databaseConnection -> error);
		}
		while (self :: $databaseConnection -> next_result()) {;} // flush multi_queries, prevents queries afterwards from not running
		if (self :: $databaseConnection -> errno){
			die('The following SQL failed to work: '.$sql.'<br />'.self :: $databaseConnection -> error);
		}
	}
	
	public static function returnLastInsertedId(){
		return self :: $databaseConnection -> insert_id;
	}
	
	public static function selectSql($sql){
		$result = self :: $databaseConnection -> query($sql);
		if($result === FALSE){
			die('The following SQL failed to work: '.$sql.'<br />'.self :: $databaseConnection -> error);
		}
		while ($row = mysqli_fetch_assoc($result)) {
			self :: $selectResult[] = $row;
		}
		return self :: $selectResult;
	}
	
	public static function resetSelectArray(){
		self :: $selectResult = array();
	}
	
	public static function update($data,$startTable='error',$theKey=''){
		if ($startTable == 'error'){
			die('Start table must have a value passed to it.');
		}
		//self :: connect();
		foreach($data as $key => $value){
			if (is_array($value)){
				if (!is_numeric($key)){
					if ($startTable == ''){
						self :: update ($value,'',$theKey.'_'.$key);
					}
					else{
						self :: update ($value,'',$startTable.'_'.$key);
					}
				}
				else{
					self :: update($value,'',$theKey);
				}
			}
			else{
				foreach($data as $keyName => $valueName){
					if($keyName == 'id'){
						$id = $valueName;
					}
				}
				if ($startTable == ''){
					if ($key != 'id'){
						$sql = self :: createSql(array($key => $value), $theKey, $id, 'update');
						if(self::$debug==true){
							echo 'Attempting SQL:'.$sql.'<BR/>'."\n";
						}
						self :: runSql($sql);
					}
				}
				else{
					if($key != 'id'){
						$sql = self :: createSql(array($key => $value), $startTable, $id, 'update');
						if(self::$debug==true){
							echo 'Attempting SQL:'.$sql.'<BR/>'."\n";
						}
						$worked = self :: runSql($sql);
						if(self::$debug==true){
							if($worked==true){
								//echo 'Update Worked'.'<BR/>'."\n";
							}else{
								echo 'Update Failed'.'<BR/>'."\n";
							}
						}
					}
				}
			}
		}
	}
	
	public static function createSql($array,$table,$optionals=array(),$command='insert'){
		//this function expects key/value pairs for the array to match the table
		if($command == "insert"){
			
			$sql = "INSERT INTO ".$table."(";
			foreach($array as $key => $value){
				if(!is_array($value)){
					$sql = $sql.$key.',';
				}
			}
			foreach($optionals as $key => $value){
				if(!is_array($value)){
					$sql = $sql.$key.',';
				}
			}
			$sql = rtrim($sql, ",");
			$sql = $sql . ') VALUES (';
			foreach($array as $key => $value){
				if(!is_array($value)){
					if($value==null){
						$sql = $sql."NULL".",";
					}else{
						$sql = $sql."'".$value."',";
					}
				}
			}
			foreach($optionals as $key => $value){
				if(!is_array($value)){
					$sql = $sql.$value.',';
				}
			}
			$sql = rtrim($sql, ",");
			$sql = $sql . ');';
		}
		
		if($command == 'update'){
		
			$sql = "UPDATE ".$table." SET ";
			foreach($array as $key => $value){
				if(!is_array($value)){
					$sql = $sql.$key."='".$value."',";
				}
			}
			$sql = rtrim($sql, ",");
			$sql = $sql." WHERE id='".$optionals."';";
		}
		return $sql;
	}
	
	public static function createMultiSql($array,$table,$optionals){
		//this expects an array of arrays that the inner array is made up of key/value pairs that match the table
		$sql = "";
		foreach($array as $arrayitem){
			$sql = $sql.self :: createSql($arrayitem,$table,$optionals);
		}
		return $sql;
	}
	
	public static function begin(){
		self :: $databaseConnection->autocommit(FALSE);
	}
	
	public static function rollback(){
		self :: $databaseConnection->rollback();
	}
	
	public static function commit(){
		self :: $databaseConnection->commit();
	}
	
	public static function connect($database = "",$host = "",$username = "",$password = ""){
		if($host==""){
			$host = Database_Config::$host;
		}
		if($username==""){
			$username = Database_Config::$username;
		}
		if($password==""){
			$password = Database_Config::$password;
		}
		if($database==""){
			$database = Database_Config::$database;
		}
		
		if(!isset(self :: $databaseConnection)){
			self :: $databaseConnection = new mysqli($host, $username, $password, $database);
			if(self :: $databaseConnection -> connect_errno){
				printf("Connect failed: %s\n", self :: $databaseConnection -> connect_error);
				die();
			}
		}
	}
	
	public static function disconnect(){
		self :: $databaseConnection -> close();
	}
}
?>